#

siesta_subtest(ts_n_terminal_elec_x LABELS simple)
siesta_subtest(ts_n_terminal_elec_z LABELS simple)
siesta_subtest(ts_n_terminal_3 EXTRA_FILES ts_n_terminal_elec_x.TSHS ts_n_terminal_elec_z.TSHS)
siesta_subtest(ts_n_terminal_3 NAME_ALTER ts_n_terminal_3_tbt
  EXEC ${PROJECT_NAME}.tbtrans
  EXTRA_FILES ts_n_terminal_elec_x.TSHS ts_n_terminal_elec_z.TSHS ts_n_terminal_3.TSHS)
