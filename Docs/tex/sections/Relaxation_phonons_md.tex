

This functionality is not \siesta-specific, but is implemented to
provide a more complete simulation package. The program has an outer
geometry loop: it computes the electronic structure (and
thus the forces and stresses) for a given geometry, updates the
atomic positions (and maybe the cell vectors) accordingly and moves on
to the next cycle.
%
If there are molecular dynamics options missing you are highly
recommend to look into \fdf{MD.TypeOfRun:Lua} or
\fdf{MD.TypeOfRun:Master}.


Several options for MD and structural optimizations are
implemented, selected by
\begin{fdfentry}{MD.TypeOfRun}[string]<CG>

  \begin{fdfoptions}

    \option[CG]%
    \fdfindex*{MD.TypeOfRun:CG}%
    Performs an atomic coordinates optimization by using the conjugate gradients
    method. If \fdf{MD.VariableCell} is enabled (see below), the optimization
    includes the cell vectors.

    \option[Broyden]%
    \fdfindex*{MD.TypeOfRun:Broyden}%
    Performs an atomic coordinates optimization by using a modified Broyden
    method, which falls within the Quasi-Newton family of algorithms. If
    \fdf{MD.VariableCell} is enabled (see below), the optimization includes
    the cell vectors.

    \option[FIRE]%
    \fdfindex*{MD.TypeOfRun:FIRE}%
    Performs an atomic coordinates optimization by using the Fast Inertial
    Relaxation Engine (E. Bitzek et al, PRL 97, 170201, (2006)). If
    \fdf{MD.VariableCell} is enabled (see below), the optimization includes
    the cell vectors.
    FIRE avoids the need for linear search, thus making each individual iteration
    faster when compared to Quasi-Newton methods. However, it also needs more
    iterations to converge, so its efficiency is system-dependent.

    \option[Verlet]%
    \fdfindex*{MD.TypeOfRun:Verlet}%
    Standard Velocity-Verlet algorithm for NVE molecular dynamics.

    \option[Nose]%
    \fdfindex*{MD.TypeOfRun:Nose}%
    Constant temperature (NVT) MD with using a Nos\'e thermostat.

    \option[ParrinelloRahman]%
    \fdfindex*{MD.TypeOfRun:ParrinelloRahman}%
    Constant pressure (NPE) MD, controlled by the Parrinello-Rahman method.

    \option[NoseParrinelloRahman]%
    \fdfindex*{MD.TypeOfRun:NoseParrinelloRahman}%
    Constant temperature and pressure (NPT) MD using both methods above, the
    Nos\'e thermostat and the Parrinello-Rahman method.

    \option[Anneal]%
    \fdfindex*{MD.TypeOfRun:Anneal}%
    Constant temperature and/or pressure MD (see the variable
    \fdf{MD.AnnealOption} below), using a very simple velocity rescaling method
    (i.e. Berendsen thermostat and/or barostat \cite{Berendsen84}). It can be
    used to quickly equilibrate a system to a desired temperature and pressure.
    However, atomic velocities resulting from this option are non-canonical and
    thus tend to produce physically-inaccurate results. Therefore, one must
    rely on the Nos\'e and/or ParrinelloRahman options for production MD runs
    after the equilibration is done.

    \option[FC]%
    \fdfindex*{MD.TypeOfRun:FC}%
    Compute force constants matrix\index{Force Constants Matrix} for
    phonon calculations.

    \option[Master|Forces]%
    \fdfindex*{MD.TypeOfRun:Master}%
    \fdfindex*{MD.TypeOfRun:Forces}%
    Receive coordinates from, and return forces to, an external
    driver program, using MPI, Unix pipes, or Inet sockets for
    communication. The routines in module \program{fsiesta} allow the
    user's program to perform this communication transparently, as if
    \siesta\ were a conventional force-field subroutine. See
    \shell{Util/SiestaSubroutine/README} for details. WARNING: if this
    option is specified without a driver program sending data, siesta
    may hang without any notice.

    See directory \shell{Util/Scripting} \index{Scripting} for other driving
    options.

    \option[Lua]%
    \fdfindex*{MD.TypeOfRun:Lua}%
    Fully control the MD cycle and convergence path using an external
    Lua script.

    With an external Lua script one may control nearly everything from
    a script. One can query \emph{any} internal data-structures in
    \siesta\ and, similarly, return \emph{any} data thus overwriting
    the internals. A list of ideas which may be implemented in such a
    Lua script are:
    \begin{itemize}
      \item New geometry relaxation algorithms

      \item NEB calculations

      \item New MD routines

      \item Convergence tests of \fdf{Mesh!Cutoff} and
      \fdf{kgrid.MonkhorstPack}, or other parameters (currently basis
      set optimizations cannot be performed in the Lua script).

    \end{itemize}
    Sec.~\ref{sec:lua} for additional details (and a description of
    \program{flos} which implements some of the above mentioned items).

    Using this option requires the compilation of \siesta\ with the
    \program{flook} library.%
    \index{flook}\index{External library!flook}%
    If \siesta\ is not compiled as prescribed in Sec.~\ref{sec:libs}
    this option will make \siesta\ die.

    \option[TDED]%
    \fdfindex*{MD.TypeOfRun:TDED}%

    New option to perform time-dependent electron dynamics simulations
    (TDED) within RT-TDDFT. For more details see
    Sec.~\ref{sec:tddft}.

    The second run of \siesta\ uses this option with the files
    \sysfile{TDWF} and \sysfile{TDXV} present in the working
    directory.  In this option ions and electrons are assumed to move
    simultaneously. The occupied electronic states are time-evolved
    instead of the usual SCF calculations in each step.  Choose this
    option even if you intend to do only-electron dynamics. If you
    want to do an electron dynamics-only calculation set
    \fdf{MD.FinalTimeStep} equal to $1$. For optical response
    calculations switch off the external field during the second
    run. The \fdf{MD.LengthTimeStep}, unlike in the standard MD
    simulation, is defined by mulitpilication of \fdf{TDED!TimeStep}
    and \fdf{TDED!Nsteps}. In TDDFT calculations, the user defined
    \fdf{MD.LengthTimeStep} is ignored.


  \end{fdfoptions}

  \note if \fdf{Compat!Pre-v4-Dynamics} is \fdftrue\ this will default
  to \fdf*{Verlet}.

  Note that some options specified in later variables (like quenching)
  modify the behavior of these MD options.

  Appart from being able to act as a force subroutine for a driver
  program that uses module fsiesta, \siesta\ is also prepared to
  communicate with the i-PI code (see
  \url{https://github.com/i-pi/i-pi}).
  To do this, \siesta\ must be started after i-PI (it acts as a client
  of i-PI, communicating with it through Inet or Unix sockets), and
  the following lines must be present in the .fdf data file:
  \begin{fdfexample}
     MD.TypeOfRun      Master     # equivalent to 'Forces'
     Master.code       i-pi       # ( fsiesta | i-pi )
     Master.interface  socket     # ( pipes | socket | mpi )
     Master.address    localhost  # or driver's IP, e.g. 150.242.7.140
     Master.port       10001      # 10000+siesta_process_order
     Master.socketType inet       # ( inet | unix )
  \end{fdfexample}

\end{fdfentry}



\subsection{Compatibility with pre-v4 versions}
\index{Backward compatibility}

\input{tex/sections/Relaxation_phonons_md/Compatibility.tex}

\subsection{Structural relaxation}

\input{tex/sections/Relaxation_phonons_md/Structural_relaxation.tex}

\subsection{Target stress options}

\input{tex/sections/Relaxation_phonons_md/Target_stress.tex}

\subsection{Molecular dynamics}

\input{tex/sections/Relaxation_phonons_md/Molecular_dynamics.tex}

\subsection{Output options for dynamics}

\input{tex/sections/Relaxation_phonons_md/Output.tex}

\subsection{Restarting geometry optimizations and MD runs}

\input{tex/sections/Relaxation_phonons_md/Restarting.tex}

\subsection{Use of general constraints}

\input{tex/sections/Relaxation_phonons_md/Constraints.tex}

\subsection{Phonon calculations}

\input{tex/sections/Relaxation_phonons_md/Phonons.tex}
