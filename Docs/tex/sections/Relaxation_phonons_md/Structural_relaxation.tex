In this mode of operation, the program moves the atoms (and optionally
the cell vectors) trying to minimize the forces (and stresses) on
them.

These are the options common to all relaxation methods. If the Zmatrix
input option is in effect (see Sec.~\ref{sec:Zmatrix}) the
Zmatrix-specific options take precedence.  The 'MD' prefix is
misleading but kept for historical reasons.

\begin{fdflogicalF}{MD.VariableCell}
  \index{cell relaxation}

  If \fdftrue, the lattice is relaxed together with the atomic
  coordinates. It allows to target hydrostatic pressures or arbitrary
  stress tensors. See \fdf{MD.MaxStressTol},
  \fdf{Target!Pressure}, \fdf{Target!Stress.Voigt},
  \fdf{Constant!Volume}, and
  \fdf{MD.PreconditionVariableCell}.

  \note only compatible with \fdf{MD.TypeOfRun:CG},
  \fdf*{Broyden} or \fdf*{fire}.

\end{fdflogicalF}


\begin{fdflogicalF}{Constant!Volume}
  \fdfindex*{MD.ConstantVolume}
  \fdfdeprecates{MD.ConstantVolume}%
  \index{constant-volume cell relaxation}

  If \fdftrue, the cell volume is kept constant in a variable-cell
  relaxation: only the cell shape and the atomic coordinates are
  allowed to change.  Note that it does not make much sense to specify
  a target stress or pressure in this case, except for anisotropic
  (traceless) stresses.  See \fdf{MD.VariableCell},
  \fdf{Target.Stress.Voigt}.

  \note only compatible with \fdf{MD.TypeOfRun:CG},
  \fdf*{Broyden} or \fdf*{fire}.

\end{fdflogicalF}

\begin{fdflogicalF}{MD.RelaxCellOnly}
  \index{relaxation of cell parameters only}

  If \fdftrue, only the cell parameters are relaxed (by the Broyden or
  FIRE method, not CG). The atomic coordinates are re-scaled to the
  new cell, keeping the fractional coordinates constant. For
  \fdf{Zmatrix} calculations, the fractional position of the first
  atom in each molecule is kept fixed, and no attempt is made to
  rescale the bond distances or angles.

  \note only compatible with \fdf{MD.TypeOfRun:Broyden} or \fdf*{fire}.

\end{fdflogicalF}

\begin{fdfentry}{MD.MaxForceTol}[force]<$0.04\,\mathrm{eV/Ang}$>

  Force tolerance in coordinate optimization.
  Run stops if the maximum atomic force is
  smaller than \fdf{MD.MaxForceTol} (see \fdf{MD.MaxStressTol}
  for variable cell).

\end{fdfentry}

\begin{fdfentry}{MD.MaxStressTol}[pressure]<$1\,\mathrm{GPa}$>

  Stress tolerance in variable-cell CG optimization. Run stops if the
  maximum atomic force is smaller than \fdf{MD.MaxForceTol} and the
  maximum stress component is smaller than \fdf{MD.MaxStressTol}.

  Special consideration is needed if used with Sankey-type basis sets,
  since the combination of orbital kinks at the cutoff radii and the
  finite-grid integration originate discontinuities in the stress
  components, whose magnitude depends on the cutoff radii (or energy
  shift) and the mesh cutoff. The tolerance has to be larger than the
  discontinuities to avoid endless optimizations if the target stress
  happens to be in a discontinuity.

\end{fdfentry}

\begin{fdfentry}{MD.Steps}[integer]<0>
  \fdfindex*{MD.NumCGsteps}
  \fdfdeprecates{MD.NumCGsteps}

  Maximum number of steps in a minimization routine
  (the minimization will stop if tolerance is reached before; see
  \fdf{MD.MaxForceTol} below).

  \note The old flag \fdf{MD.NumCGsteps} will remain for historical
  reasons.

\end{fdfentry}

\begin{fdfentry}{MD.MaxDispl}[length]<$0.2\,\mathrm{Bohr}$>
  \fdfindex*{MD.MaxCGDispl}
  \fdfdeprecates{MD.MaxCGDispl}

  Maximum atomic displacements in an optimization move.

  In the Broyden optimization method, it is also possible to limit
  indirectly the \textit{initial\/} atomic displacements using
  \fdf{MD.Broyden.Initial.Inverse.Jacobian}. For the \fdf*{FIRE} method, the
  same result can be obtained by choosing a small time step.

  Note that there are Zmatrix-specific options that override this option.

  \note The old flag \fdf{MD.MaxCGDispl} will remain for historical
  reasons.

\end{fdfentry}

\begin{fdfentry}{MD.PreconditionVariableCell}[length]<$5\,\mathrm{Ang}$>

  A length to multiply to the strain components in a variable-cell
  optimization. The strain components enter the minimization on the
  same footing as the coordinates. For good efficiency, this length
  should make the scale of energy variation with strain similar to the
  one due to atomic displacements. It is also used for the application
  of the \fdf{MD.MaxDispl} value to the strain components.

\end{fdfentry}


\begin{fdfentry}{ZM.ForceTolLength}[force]<$0.00155574\,\mathrm{Ry/Bohr}$>

  Parameter that controls the convergence with respect to forces on
  Z-matrix lengths

\end{fdfentry}


\begin{fdfentry}{ZM.ForceTolAngle}[torque]<$0.00356549\,\mathrm{Ry/rad}$>

  Parameter that controls the convergence with respect to forces on
  Z-matrix angles

\end{fdfentry}

\begin{fdfentry}{ZM.MaxDisplLength}[length]<$0.2\,\mathrm{Bohr}$>

  Parameter that controls the maximum change in a Z-matrix length
  during an optimisation step.

\end{fdfentry}

\begin{fdfentry}{ZM.MaxDisplAngle}[angle]<$0.003\,\mathrm{rad}$>

  Parameter that controls the maximum change in a Z-matrix angle
  during an optimisation step.

\end{fdfentry}



\subsubsection{Conjugate-gradients optimization}

This was historically the default geometry-optimization method, and
all the above options were introduced specifically for it, hence their
names. The following pertains only to this method:

\index{Conjugate-gradient history information}
\begin{fdflogicalF}{MD.UseSaveCG}
  \index{reading saved data!CG}

  Instructs to read the conjugate-gradient hystory information stored
  in file \sysfile{CG} by a previous run.

  \note to get actual continuation of iterrupted CG runs, use
  together with \fdf{MD.UseSaveXV} \fdftrue\ with the \sysfile*{XV}
  file generated in the same run as the CG file.  If the required file
  does not exist, a warning is printed but the program does not
  stop. Overrides \fdf{UseSaveData}.

  \note no such feature exists yet for a Broyden-based relaxation.

\end{fdflogicalF}

\subsubsection{Broyden optimization}

It uses the modified Broyden algorithm to
build up the Jacobian matrix. (See D.D. Johnson, PRB 38, 12807
(1988)). (Note: This is not BFGS.)

\begin{fdfentry}{MD.Broyden!History.Steps}[integer]<$5$>
  \index{Broyden optimization}

  Number of relaxation steps during which the modified Broyden
  algorithm builds up the Jacobian matrix.

\end{fdfentry}

\begin{fdflogicalT}{MD.Broyden!Cycle.On.Maxit}

  Upon reaching the maximum number of history data sets which are kept
  for Jacobian estimation, throw away the oldest and shift the rest to
  make room for a new data set. The alternative is to re-start the
  Broyden minimization algorithm from a first step of a diagonal
  inverse Jacobian (which might be useful when the minimization is
  stuck).

\end{fdflogicalT}

\begin{fdfentry}{MD.Broyden!Initial.Inverse.Jacobian}[real]<$1$>

  Initial inverse Jacobian for the optimization procedure. (The units
  are those implied by the internal \siesta\ usage. The default value
  seems to work well for most systems.

\end{fdfentry}



\subsubsection{FIRE relaxation}

Implementation of the Fast Inertial Relaxation Engine (FIRE) method
(E. Bitzek et al, PRL 97, 170201, (2006) in a manner compatible with
the CG and Broyden modes of relaxation. (An older implementation
activated by the \fdf*{MD.FireQuench} variable is still available).

\begin{fdfentry}{MD.FIRE.TimeStep}[time]<\fdfvalue{MD.LengthTimeStep}>

  The (fictitious) time-step for FIRE relaxation.  This is the main
  user-variable when the option \fdf*{FIRE} for
  \fdf{MD.TypeOfRun} is active.

  \note the default value is encouraged to be changed as the link to
  \fdf{MD.LengthTimeStep} is misleading.

  There are other low-level options tunable by the user (see the
  routines \texttt{fire\_optim} and \texttt{cell\_fire\_optim} for
  more details.

\end{fdfentry}


\ifdeprecated
% The below options are deprecated in favor of:
% MD.TypeOfRun fire

\subsubsection{Quenched MD}

These methods are really based on molecular dynamics, but are used for
structural relaxation.

Note that the Zmatrix input option (see Sec.~\ref{sec:Zmatrix}) is not
compatible with molecular dynamics. The initial geometry can be
specified using the Zmatrix format, but the Zmatrix generalized
coordinates will not be updated.

Note also that the force and stress tolerances have no effect on
the termination conditions of these methods. They run for the number
of MD steps requested (this is arguably a bug).

\begin{fdflogicalF}{MD.Quench}

  Logical option to perform a power quench during the molecular
  dynamics.  In the power quench, each velocity component is set to
  zero if it is opposite to the corresponding force of that
  component. This affects atomic velocities, or unit-cell velocities
  (for cell shape optimizations).

  \note only applicable for \fdf{MD.TypeOfRun:Verlet} or
  \fdf*{ParrinelloRahman}.
  %
  It is incompatible with Nose thermostat options.

  \note \fdf{MD.Quench} is superseded by \fdf{MD.FireQuench} (see
  below).

\end{fdflogicalF}


\begin{fdflogicalF}{MD.FireQuench}

  See the new option \fdf*{FIRE} for \fdf{MD.TypeOfRun}.

  Logical option to perform a FIRE quench during a Verlet molecular
  dynamics run, as described by Bitzek \textit{et al.} in
  Phys. Rev. Lett. \textbf{97}, 170201 (2006). It is a relaxation
  algorithm, and thus the dynamics are of no interest per se: the
  initial time-step can be played with (it uses
  \fdf{MD.LengthTimeStep} as initial $\Delta t$), as well as the
  initial temperature (recommended 0) and the atomic masses
  (recommended equal). Preliminary tests seem to indicate that the
  combination of $\Delta t = 5$ fs and a value of 20 for the atomic
  masses works reasonably. The dynamics stops when the force tolerance
  is reached (\fdf{MD.MaxForceTol}). The other parameters
  controlling the algorithm (initial damping, increase and decrease
  thereof etc.) are hardwired in the code, at the recommended values
  in the cited paper, including $\Delta t_{max} = 10$ fs.

  Only available for \fdf{MD.TypeOfRun:Verlet}
  It is incompatible with Nose thermostat options. No variable
  cell option implemented for this at this stage.
  \fdf{MD.FireQuench} supersedes \fdf{MD.Quench}. This option is
  deprecated. The new option \fdf*{FIRE} for \fdf{MD.TypeOfRun} should be
  used instead.

\end{fdflogicalF}


\fi