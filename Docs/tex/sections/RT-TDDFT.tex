
Now it is possible to perform Real-Time Time-Dependent Density
Functional Theory (RT-TDDFT)-based calculations using the
\siesta\ method. This section includes a brief introduction to the
TDDFT method and implementation, shows how to run the TDDFT-based
calculations, and provides a reference guide to the additional input
options.

\subsection{Brief description}
The basic features of the TDDFT have been implemented
within the \siesta\ code. Most of the
details can be found in the paper Phys. Rev. B
\textbf{66} 235416 (2002), by A. Tsolakidis,
D. S\'anchez-Portal and, Richard M. Martin.
However, the practical implementation of the present version
is very different
from the initial version. The present implementation of the TDDFT has been
programmed with the primary aim of calculating the optical
response of clusters and solids, however, it has been successfully used to
calculate the electronic stopping power of solids as well.

For the calculation of the optical response of the electronic
systems a perturbation
to the system is applied at time step 0, and the system is
allowed to reach a self-consistent solution. Then, the perturbation
is switched off for all subsequent time steps, and the electrons
are allowed to evolve according to time-dependent Kohn-Sham
equations. For the case of a cluster the perturbation
is a finite (small) electric field. For the case of bulk
material (not yet fully implemented) the initial perturbation
is different but the main strategy is similar.

The present version of the RT-TDDFT implementation is also capable of
performing a simultaneous  dynamics of electrons and
ions but this is limited to the cases in which forces on the ions are within
ignorable limit.

The general working scheme is as following. First, the system is
allowed to reach a self-consistent solution for some initial
conditions (for example an initial ionic configuration or an applied
external field). The occupied Kohn-Sham orbitals (KSOs) are then selected
and stored in memory.  The occupied KSOs are then made to evolve in
time, and the Hamiltonian is recalculated for each time step.

\subsection{Partial Occupations}

This is a note of caution. This implementation of RT-TDDFT can not propagate partially occupied orbitals. While partial occupation of states is a common occurrence, they must be avoided. The issue of partially occupied states becomes, particularly,  tricky when dealing with metals and k-point sampling at the same time. The code tries to detect partial occupations and stops during the first run but it is not guarantied. Consequently, it can lead to additional or missing charge. Ultimately it is users' responsibility to make sure that the system has no partial occupations and missing or added charge. There are different ways to avoid partial occupations depending on the system and simulation parameters; for example changing spin-polarization and/or adding some k-point shift to k-points.

\subsection{Input options for RT-TDDFT}

A TDDFT calculation requires two runs of \siesta. In the first run
with appropriate flags it calculates the self-consistent initial
state, i.e., only occupied initial KSOs stored in \sysfile{TDWF}
file. The second run uses this file and the structure
file \sysfile{TDXV} as input and evolves the occupied
KSOs.

\begin{fdflogicalF}{TDED!WF.Initialize}

If set to \fdftrue\ in a standard self-consistent \siesta\ calculation, it
makes the program save the KSOs after reaching
self-consistency. This constitutes the first run.

\end{fdflogicalF}

\begin{fdfentry}{TDED!Nsteps}[integer]<1>

Number of electronic time steps between each atomic movement. It can not be less
than $1$.

\end{fdfentry}
\begin{fdfentry}{TDED!TimeStep}[time]<$0.001\,\mathrm{fs}$>
Length of time for each electronic step. The default value is only suggestive. Users must
determine an appropriate value for the electronic time step.

\end{fdfentry}

\begin{fdflogicalF}{TDED!Extrapolate}
  An extrapolated Hamiltonian is applied to evolve KSOs for \fdf{TDED!Extrapolate.Substeps} number of substeps within a sinlge electronic step without re-evaluating the Hamiltonian.
\end{fdflogicalF}

\begin{fdfentry}{TDED!Extrapolate.Substeps}[integer]<3>
  Number of electronic substeps when an extrapolated Hamiltonian is applied
  to propogate the KSOs. Effective only when \fdf{TDED!Extrapolate} set to be true.
\end{fdfentry}


\begin{fdflogicalT}{TDED!Inverse.Linear}

  If \fdftrue\ the inverse of matrix
  \begin{equation}
    \mathbf{S}+\mathrm{i}\mathbf{H}(t)\frac{\mathrm dt}{2}
  \end{equation}
  is calculated by solving a system of linear equations which
  implicitly multiplies the inverted matrix to the right hand side
  matrix. The alternative is explicit inversion and multiplication.
  The two options may differ in performance.

\end{fdflogicalT}

\begin{fdflogicalF}{TDED!WF.Save}

  Option to save wavefunctions at the end of a simulation for a
  possible restart or analysis. Wavefunctions are saved in file
  \sysfile{TDWF}. A TDED restart requires \sysfile{TDWF},
  \sysfile{TDXV}, and \sysfile{VERLET\_RESTART} from the previous
  run.  The first step of the restart is same as the last of the
  previous run.

\end{fdflogicalF}

 \begin{fdflogicalT}{TDED!Write.Etot}

If \fdftrue\ the total energy for every time step is stored in the file
\sysfile{TDETOT}.

\end{fdflogicalT}

 \begin{fdflogicalF}{TDED!Write.Dipole}

If \fdftrue\ a file \sysfile{TDDIPOL} is created that can be
further processed to calculate polarizability.

\end{fdflogicalF}

 \begin{fdflogicalF}{TDED!Write.Eig}

If \fdftrue\ the quantities $\langle \phi(t)|H(t)|\phi(t)\rangle$ in every
time step are calculated and stored in the file
\sysfile{TDEIG}. This is not trivial, hence can increase computational time.

\end{fdflogicalF}

\begin{fdflogicalF}{TDED!Saverho}

  If \fdftrue\ the instantaneous time-dependent density is saved to
  \file{<istep>.TDRho} after every \fdf{TDED!Nsaverho} number of
  steps.

\end{fdflogicalF}

\begin{fdfentry}{TDED!Nsaverho}[integer]<100>

Fixes the number of steps of ion-electron dynamics after which the
instantaneous time-dependent density is saved. May require a lot of
disk space.

\end{fdfentry}

